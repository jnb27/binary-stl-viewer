# STL File Viewer

## About
The STL (standard tesselation/triangle language) file format is used in 3D printing. It specifies triangles in a 3D model, including a normal vector and three vertices for each triangle. These files can be taken and "sliced" into many layers in order to generate a toolpath for 3D printers. This project renders 3D models from binary STL files using OSG.

## STL Binary File Format
- 80 byte header
-  4 byte unsigned int - # of triangles
- Each triangle:
    - 3x 4 byte floats - normal vector
    - 3x 4 byte floats - vertex one
    - 3x 4 byte floats - vertex two
    - 3x 4 byte floats - vertex three
    - 2 byte short unsigned int - attribute byte count

## Dependencies
- OpenSceneGraph (OSG)

## Build Instructions
In project directory:
```
mkdir build
cd build
cmake ..
make
```

## Usage
``` ./stlviewer [option] [stl file] ```

Options:

    -w render wireframe only

    -g render solid model triangles with gradient instead of wireframe