#include <iostream>
#include <fstream>
#include <cstring> // strcmp

#include <osg/Referenced>
#include <osg/Geometry>
#include <osg/Geode>
#include <osg/Array>
#include <osgViewer/Viewer>
#include <osg/LineWidth>



// contents of each triangle entry
struct Triangle{
    float n1, n2, n3;
    float v1x, v1y, v1z;
    float v2x, v2y, v2z;
    float v3x, v3y, v3z;
    uint16_t attr_byte_count;
};


// add solid model out of triangles to root group, with or without a gradient
// on each triangle
void addModel(osg::ref_ptr<osg::Group> root, std::vector<Triangle> triangles, bool gradient);

// add a wireframe out of triangles to the root group
void addWireFrame(osg::ref_ptr<osg::Group> root, std::vector<Triangle> triangles);


int main(int argc, char *argv[]){

    //***** CL args *****//

    // default values (no extra options)
    bool use_gradient = false;
    bool use_solid = true;
    bool use_wireframe = true;

    // wireframe only
    if(argc == 3 && strcmp(argv[1], "-w") == 0){
        use_solid = false;
        use_wireframe = true;
    }
    // gradient instead of wireframe
    else if(argc == 3 && strcmp(argv[1], "-g") == 0){
        use_gradient = true;
        use_wireframe = false;
    }
    // default - no gradient solid + wireframe
    else if(argc != 2){
        std::cout << "Usage: ./stlviewer [-w/-g/none] [binary stl file]" << std::endl;
        return EXIT_FAILURE;
    }


    //***** Open and read binary STL file *****//

    // open stl file
    std::fstream file(argv[argc-1], std::ios::binary | std::ios::in);

    if(!file){
        std::cout << "File error." << std::endl;
        return EXIT_FAILURE;
    }

    // Binary STL file format:
    //     80 byte header
    //      4 byte unsigned int little endian - # of triangles
    //     Each triangle:
    //         3x 4 byte floats - normal vector
    //         3x 4 byte floats - vertex one
    //         3x 4 byte floats - vertex two
    //         3x 4 byte floats - vertex three
    //         2 byte short unsigned - attribute byte count

    uint8_t header[80];
    uint32_t num_triangles;
    std::vector<Triangle> triangles;

    // read header and triangle count
    file.read((char *)header, sizeof(header));
    file.read((char *)&num_triangles, sizeof(num_triangles));
    
    // read in each triangle
    for(int i = 0; i < num_triangles; i++){

        Triangle t;

        // normal vector
        file.read((char *)&t.n1, sizeof(t.n1));
        file.read((char *)&t.n2, sizeof(t.n2));
        file.read((char *)&t.n3, sizeof(t.n3));

        // vertex one
        file.read((char *)&t.v1x, sizeof(t.v1x));
        file.read((char *)&t.v1y, sizeof(t.v1y));
        file.read((char *)&t.v1z, sizeof(t.v1z));

        // vertex two
        file.read((char *)&t.v2x, sizeof(t.v2x));
        file.read((char *)&t.v2y, sizeof(t.v2y));
        file.read((char *)&t.v2z, sizeof(t.v2z));

        // vertex three
        file.read((char *)&t.v3x, sizeof(t.v3x));
        file.read((char *)&t.v3y, sizeof(t.v3y));
        file.read((char *)&t.v3z, sizeof(t.v3z));

        // attribute byte count
        file.read((char *)&t.attr_byte_count, sizeof(t.attr_byte_count));

        triangles.push_back(t);
    }


    //***** render using OSG *****//

    osg::ref_ptr<osg::Group> root = new osg::Group;

    if(use_solid){
        addModel(root, triangles, use_gradient);
    }

    if(use_wireframe){
        addWireFrame(root, triangles);
    }

    osgViewer::Viewer viewer;
    viewer.setSceneData(root);
    return viewer.run();
}



void addModel(osg::ref_ptr<osg::Group> root, std::vector<Triangle> triangles, bool gradient){

    osg::ref_ptr<osg::Geometry> model = new osg::Geometry;
    osg::ref_ptr<osg::Vec3Array> vertices = new::osg::Vec3Array;
    osg::ref_ptr<osg::Vec4Array> colors = new osg::Vec4Array;
    
    // add vertices and colors if gradient is used
    for(int i = 0; i < triangles.size(); i++){
        vertices->push_back(osg::Vec3(triangles[i].v1x, triangles[i].v1y, triangles[i].v1z));
        vertices->push_back(osg::Vec3(triangles[i].v2x, triangles[i].v2y, triangles[i].v2z)); 
        vertices->push_back(osg::Vec3(triangles[i].v3x, triangles[i].v3y, triangles[i].v3z)); 

        if(gradient){
            colors->push_back(osg::Vec4(0.3, 0.3, 1.0, 1.0));
            colors->push_back(osg::Vec4(0.35,0.35,1.0,1.0));
            colors->push_back(osg::Vec4(0.4,0.4,1.0,1.0));
        }
    }

    model->setVertexArray(vertices);
    model->setColorArray(colors);

    // set color binding dependent on gradient use
    if(gradient){
        model->setColorBinding(osg::Geometry::BIND_PER_VERTEX);
    }
    else{
        colors->push_back(osg::Vec4(0.35, 0.35, 1.0, 1.0));
        model->setColorBinding(osg::Geometry::BIND_OVERALL);
    }

    model->getOrCreateStateSet()->setMode(GL_LIGHTING, osg::StateAttribute::OFF);
    model->getOrCreateStateSet()->setMode(GL_BLEND, osg::StateAttribute::ON);

    // every trio of vertices is a new triangle
    model->addPrimitiveSet(new osg::DrawArrays(osg::PrimitiveSet::TRIANGLES, 0, vertices->size()));

    osg::ref_ptr<osg::Geode> geode = new osg::Geode;
    geode->addDrawable(model);
    root->addChild(geode);
}

void addWireFrame(osg::ref_ptr<osg::Group> root, std::vector<Triangle> triangles){

    osg::ref_ptr<osg::Geometry> wireframe = new osg::Geometry;
    osg::ref_ptr<osg::Vec3Array> vertices = new::osg::Vec3Array;
    osg::ref_ptr<osg::Vec4Array> colors = new osg::Vec4Array;

    // add vertices - each pair of vertices is one line
    for(int i = 0; i < triangles.size(); i++){
        // since triangles are touching, all lines will be written twice on a closed 3D object
        // simple app, not overly concerned, as long as everything is rendered correctly

        // v1 to v2
        vertices->push_back(osg::Vec3(triangles[i].v1x, triangles[i].v1y, triangles[i].v1z));
        vertices->push_back(osg::Vec3(triangles[i].v2x, triangles[i].v2y, triangles[i].v2z)); 
        
        // v2 to v3
        vertices->push_back(osg::Vec3(triangles[i].v2x, triangles[i].v2y, triangles[i].v2z)); 
        vertices->push_back(osg::Vec3(triangles[i].v3x, triangles[i].v3y, triangles[i].v3z)); 

        // v3 to v1
        vertices->push_back(osg::Vec3(triangles[i].v3x, triangles[i].v3y, triangles[i].v3z));   
        vertices->push_back(osg::Vec3(triangles[i].v1x, triangles[i].v1y, triangles[i].v1z));
    }

    // black
    colors->push_back(osg::Vec4(0.0, 0.0, 0.0, 1.0));

    wireframe->setVertexArray(vertices);
    wireframe->setColorArray(colors);
    wireframe->setColorBinding(osg::Geometry::BIND_OVERALL);

    wireframe->getOrCreateStateSet()->setMode(GL_LIGHTING, osg::StateAttribute::OFF);
    wireframe->getOrCreateStateSet()->setMode(GL_BLEND, osg::StateAttribute::ON);

    // each pair of vertices is a new line
    wireframe->addPrimitiveSet(new osg::DrawArrays(osg::PrimitiveSet::LINES, 0, vertices->size()));

    osg::ref_ptr<osg::Geode> geode = new osg::Geode;

    // make lines thicker
    osg::ref_ptr<osg::LineWidth> linewidth = new osg::LineWidth();
    linewidth->setWidth(2.f);
    geode->getOrCreateStateSet()->setAttributeAndModes(linewidth,osg::StateAttribute::ON); 

    geode->addDrawable(wireframe);
    root->addChild(geode);
}